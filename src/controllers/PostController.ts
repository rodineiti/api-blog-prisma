import { Request, Response } from "express";
import { PostService } from "../services/PostService";

class PostController {
  async index(request: Request, response: Response) {
    const users = await new PostService().index();

    return response.json(users);
  }

  async show(request: Request, response: Response) {
    const user = await new PostService().show(Number(request.params.id));

    return response.json(user);
  }

  async store(request: Request, response: Response) {
    const user = await new PostService().store(request.body);

    return response.json(user);
  }

  async update(request: Request, response: Response) {
    const user = await new PostService().update(
      Number(request.params.id),
      request.body
    );

    return response.json(user);
  }

  async destroy(request: Request, response: Response) {
    await new PostService().destroy(Number(request.params.id));

    return response.status(204).json({ message: "user removed" });
  }
}

export { PostController };

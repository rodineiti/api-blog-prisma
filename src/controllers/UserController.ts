import { Request, Response } from "express";
import { UserService } from "../services/UserService";

class UserController {
  async index(request: Request, response: Response) {
    const users = await new UserService().index();

    return response.json(users);
  }

  async show(request: Request, response: Response) {
    const user = await new UserService().show(Number(request.params.id));

    return response.json(user);
  }

  async store(request: Request, response: Response) {
    const user = await new UserService().store(request.body);

    return response.json(user);
  }

  async update(request: Request, response: Response) {
    const user = await new UserService().update(
      Number(request.params.id),
      request.body
    );

    return response.json(user);
  }

  async destroy(request: Request, response: Response) {
    await new UserService().destroy(Number(request.params.id));

    return response.status(204).json({ message: "user removed" });
  }
}

export { UserController };

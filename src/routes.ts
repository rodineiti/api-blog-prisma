import { Router } from "express";
import { PostController } from "./controllers/PostController";
import { UserController } from "./controllers/UserController";

const router = Router();

router.get("/users", new UserController().index);
router.get("/users/:id", new UserController().show);
router.post("/users", new UserController().store);
router.put("/users/:id", new UserController().update);
router.delete("/users/:id", new UserController().destroy);

router.get("/posts", new PostController().index);
router.get("/posts/:id", new PostController().show);
router.post("/posts", new PostController().store);
router.put("/posts/:id", new PostController().update);
router.delete("/posts/:id", new PostController().destroy);

export { router };

import { prisma } from "../config";

class PostService {
  async index(page = 1, limit = 10) {
    const skip = limit * (page - 1);

    const allPosts = await prisma.post.findMany();

    const posts = await prisma.post.findMany({
      skip,
      take: limit,
      orderBy: {
        id: "desc",
      },
    });

    return {
      posts,
      total: allPosts.length,
      total_pages: Math.ceil(allPosts.length / limit),
      current_page: page,
    };
  }

  async show(id: number) {
    const post = await prisma.post.findUnique({ where: { id } });

    const postPrevious = await prisma.post.findFirst({
      where: { created_at: { lt: post?.created_at?.toISOString() } },
      orderBy: {
        created_at: "desc",
      },
    });

    const postNext = await prisma.post.findFirst({
      where: { created_at: { gt: post?.created_at?.toISOString() } },
      orderBy: {
        created_at: "asc",
      },
    });

    return { postPrevious, post, postNext };
  }

  async store(formData: any) {
    return await prisma.post.create({ data: formData });
  }

  async update(id: number, formData: any) {
    return await prisma.post.update({ where: { id }, data: formData });
  }

  async destroy(id: number) {
    return await prisma.post.delete({ where: { id } });
  }
}

export { PostService };

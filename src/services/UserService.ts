import { prisma } from "../config";

class UserService {
  async index() {
    return await prisma.user.findMany();
  }

  async show(id: number) {
    return await prisma.user.findUnique({ where: { id } });
  }

  async store(formData: any) {
    return await prisma.user.create({ data: formData });
  }

  async update(id: number, formData: any) {
    return await prisma.user.update({ where: { id }, data: formData });
  }

  async destroy(id: number) {
    return await prisma.user.delete({ where: { id } });
  }
}

export { UserService };
